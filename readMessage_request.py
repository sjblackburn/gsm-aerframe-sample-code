import json, urllib.request, base64

#enter the application API key returned in the create application step below
applicationApiKey = '22222222-2222-2222-2222-222222222222'
#enter the account ID below
accountId = '1234'

url = 'https://longpoll.aerframe.aeris.com/notificationchannel/v2/' + accountId + '/longpoll/00169bb3-e0d5-e906-331e-9f50dda27cfb?apiKey=' + applicationApiKey

req = urllib.request.Request(url)
response = urllib.request.urlopen(req).read()

jsonResponse = json.loads(response.decode('utf-8'))

if len(jsonResponse['inboundSMSMessageNotification']) > 0:
    
    print('Message is: '+ base64.b64decode(jsonResponse['inboundSMSMessageNotification'][0]['inboundSMSMessage']['message']).decode('ascii'))

else:
    print('No message found')
    

