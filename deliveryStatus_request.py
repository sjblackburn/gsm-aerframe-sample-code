import json, urllib.request


#enter the application API key returned in the create application step below
applicationApiKey = '22222222-2222-2222-2222-222222222222'
#enter the account ID below
accountId = '1234'

url = 'https://longpoll.aerframe.aeris.com/notificationchannel/v2/' + accountId +'/longpoll/00079da0-82f0-3afd-34bf-08df7b65aa14?apiKey=' + applicationApiKey
req = urllib.request.Request(url)
response = urllib.request.urlopen(req).read()

jsonResponse = json.loads(response.decode('utf-8'))

if len(jsonResponse['deliveryInfoNotification']) > 0:
    print(jsonResponse['deliveryInfoNotification'][0]['deliveryInfo'][0]['deliveryStatus'])
else:
    print('No delivery status found')
        
