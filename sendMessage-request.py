import json, urllib.request

data = {
"address":[
"204041234567890" #replace this number with the imsi of the destination SIM
],
"senderAddress":"testApp1", #sender address is the application short name 
"outboundSMSTextMessage":{
"message":"Hello! This is a test message." #enter the message to be sent
},
"clientCorrelator":"654321",
"senderName":"AFTestClient"
}

#enter the application API key returned in the create application step below
applicationApiKey = '22222222-2222-2222-2222-222222222222'
#enter the account ID below
accountId = '1234'

url = 'https://api.aerframe.aeris.com/smsmessaging/v2/' + accountId + '/outbound/testApp1/requests?apiKey=' + applicationApiKey
header = {'Content-Type':'application/json'}

req = urllib.request.Request(url, json.dumps(data).encode('utf-8'), header)
response = urllib.request.urlopen(req)

print(response.read())

