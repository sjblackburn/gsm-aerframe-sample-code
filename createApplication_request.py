import json, urllib.request

#fill in all fields of data
data = {
'applicationName':'Test Application1',
'applicationShortName':'testApp1',
'applicationTag':'test1',
'description':'A sample AerFrame application'
}

#enter the account API key below
accountApiKey = "11111111-1111-1111-1111-111111111111"
#enter the account ID below
accountId = "1234"

url = "https://api.aerframe.aeris.com/registration/v2/" + accountId + "/applications?apiKey=" + accountApiKey

header = {'Content-Type':'application/json'}

req = urllib.request.Request(url, json.dumps(data).encode('utf-8'),header)
response = urllib.request.urlopen(req)

#response will return the unique application apiKey
print(response.read())




